-- find key input source
-- hs.hotkey.bind({'cmd'}, 'i', function()
-- 	local input_source = hs.keycodes.currentSourceID()
-- 	print(input_source)
-- end)

-- key mapping for vim
-- Convert input soruce as English and sends 'escape' if inputSource is not English.
-- Sends 'escape' if inputSource is English.
-- key bindding reference --> https://www.hammerspoon.org/docs/hs.hotkey.html
-- local inputEnglish = "com.apple.keylayout.ABC"
local inputEnglish = "com.apple.keylayout.USExtended"
local esc_bind

function convert_to_eng_with_esc()
	local inputSource = hs.keycodes.currentSourceID()
	if not (inputSource == inputEnglish) then
		hs.eventtap.keyStroke({}, 'right')
		hs.keycodes.currentSourceID(inputEnglish)
	end
	esc_bind:disable()
	hs.eventtap.keyStroke({}, 'escape')
	esc_bind:enable()
end

esc_bind = hs.hotkey.new({}, 'escape', convert_to_eng_with_esc):enable()

-- ev = hs.eventtap.new({hs.eventtap.event.types.mouseMoved}, function(e)
-- 	local sf = hs.screen.mainScreen():fullFrame()
--     local loc = e:location()
--     if loc.y < (sf.y + 2) and loc.y > (sf.y - 2) then
--         return true
--     else
--         return false
--     end
--  end):start()
